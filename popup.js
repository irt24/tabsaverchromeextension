function getOpenTabUrls(callback) {
  chrome.tabs.query({currentWindow : true}, function(tabs) {
    urls = tabs.map(function(tab) { return tab.url } );
    callback(urls);
  });
}

function getTabSetName() {
  return document.getElementById("tab_set_name").value;
}

function getValueOrLogFailure(map, key) {
  if (key in map) return map[key];
  console.log("Cannot find value for key: " + key);
}

const SAVED_TAB_SETS = "SAVED_TAB_SETS";
function getSavedTabSets() {
  tabSets = {}
  chrome.storage.local.get(SAVED_TAB_SETS, function(result) {
    savedTabSets = getValueOrLogFailure(result, SAVED_TAB_SETS);
    if (savedTabSets) tabSets = savedTabSets;
  });
  return tabSets;
}

function saveOpenTabSet() {
  getOpenTabUrls(function(urls) {
    var tabSets = getSavedTabSets();
    tabSets[getTabSetName()] = urls;
    chrome.storage.local.set({SAVED_TAB_SETS : tabSets});
  });
}

function openSavedTabSet() {
  savedTabSet = getValueOrLogFailure(getSavedTabSets(), getTabSetName());
  if (savedTabSet) {
    savedTabSet.map(function(tab_url) {
      chrome.tabs.create({ url : tab_url });
    });
  }
}

function logAllSavedTabSets() {
  console.log('Going to log all saved tabs.');
  for (var tabSetName in getSavedTabSets()) {
    console.log('Saved tab set: ' + tabSetName);
  }
}

document
    .addEventListener('DOMContentLoaded', function() { logAllSavedTabSets() });
document.getElementById("save_button")
    .addEventListener("click", function() { saveOpenTabSet() });
document.getElementById("open_button")
    .addEventListener("click", function() { openSavedTabSet() });
